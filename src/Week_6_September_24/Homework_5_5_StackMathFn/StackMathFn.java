package Week_6_September_24.Homework_5_5_StackMathFn;

import java.util.Stack;

/**
 * 
 */

/** Runs the function from p. 176 #5
 * @author Matthew Fallon
 * @since Oct 3, 2018
 *
 */
public class StackMathFn {

	/**Gets n factorial of n.
	 * @param args
	 */
	public static void main(String[] args) {
		int n = 4;
		Stack<Integer> stack = new Stack<Integer>();
		
		while (n > 0) {
			stack.push(n);
			n--;
		}
		
		int result = 1;
		
		while (!stack.isEmpty()) {
			int integer = (int) stack.pop();
			result = result * integer;
		}
		System.out.println("result = " + result);
	}

}
