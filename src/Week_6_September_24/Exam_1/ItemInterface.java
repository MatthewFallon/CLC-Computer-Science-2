package Week_6_September_24.Exam_1; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 26, 2018
 *
 */
public interface ItemInterface {

	/** Gets a description of the Item as a String.
	 * 
	 * @return A description of the Item as a String.
	 */
	public String getDescription();
	
	/** Gets the price of the Item.
	 * 
	 * @return Price of the Item as an int.
	 */
	public int getPrice();
	
	/** Returns a string that represents the item, including its description and its price.
	 * 
	 * @return String that contains a readable representation of an Item.
	 */
	public String toString();
	
	}
