package Week_6_September_24.Exam_1;

/**
 * 
 */

/** Tests Array bag with a practice hat problem.
 * 
 * @author Matthew Fallon
 * @since Sep 26, 2018
 *
 */
public class Hat {

	/** main runner class.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		ArrayBag<Name> hat = new ArrayBag<>();
		
		Name reed = new Name("Mr.", "Reed");
		Name matt = new Name("Matthew", "Fallon");
		
		
		hat.add(reed);
		hat.add(matt);
		hat.add(new Name("Jane", "Doe"));
		hat.add(reed);
		hat.add(matt);
		hat.add(reed);
		
		System.out.println("Mr. Reed is in the hat " + hat.getFrequencyOf(reed) + " times.");
		
		hat.remove();
		hat.remove();
		
		System.out.println();
		System.out.println("Contents:");
		while(!hat.isEmpty()) {
			System.out.println(hat.remove());
		}
	}

}
