package Week_6_September_24.Program_5_Stack;

import java.util.Scanner;
import java.util.Stack;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 1, 2018
 *
 */
public class Problem11Palindrome {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s1 = "A man, a plan, a canal: Panama."; //input
		Scanner in = new Scanner(System.in); //user input
		
		
		if (isPalindrome(s1)) {
			System.out.println("\"" + s1 + "\" is a palindrome.");
		} else {
			System.out.println("\"" + s1 + "\" isn't a palindrome.");
		}
		
		System.out.print("Input your own phrase to find out if it is a palindrome: ");
		
		s1 = in.nextLine();
		System.out.println();
		if (isPalindrome(s1)) {
			System.out.println("\"" + s1 + "\" is a palindrome.");
		} else {
			System.out.println("\"" + s1 + "\" isn't a palindrome.");
		}
		
		
		in.close();

	}
	
	/** Finds if the the letter only characters in the String given are organized into a palindrome. 
	 * 
	 * @param s1 String to check for a palindrome.
	 * @return boolean of 
	 */
	
	public static boolean isPalindrome (String s1) { 
		String s2 = ""; //Working String
		Stack<Character> firstHalf = new Stack<>();
		Stack<Character> secondHalf = new Stack<>();
		boolean result = false;
		
		
		
		for (char each : s1.toCharArray()) { //Removes all spaces and non letter characters from the string.
			if(Character.isAlphabetic(each)) {
				s2 += each;
			}
		}
		
		s2 = s2.toLowerCase();
		
			
		if (s2.length() % 2 == 0) {
			for (int i = 0; i < (s2.length() / 2); i++) { //runs through the first half of the array and adds to first stack.
				firstHalf.push(s2.charAt(i));
			}
			for (int i = s2.length() - 1; i >= (s2.length() / 2); i--) { //runs backwards through the second half of the array and adds each to stack.
				secondHalf.push(s2.charAt(i));
			}
			for (int i = 0; i < (s2.length() / 2); i++) { //checks through both stacks to find if each letter matches.
				if (firstHalf.pop() == secondHalf.pop()) {
					result = true;
					continue;
				} else {//end loop if anything turns out false to preserve result.
					result = false;
					break;
				}
			} 
		}
		
		else {
			for (int i = 0; i < (s2.length() / 2); i++) { //runs through the first half of the array and adds to first stack.
				firstHalf.push(s2.charAt(i));
			}
			for (int i = s2.length() - 1; i > (s2.length() / 2); i--) { //runs backwards through the second half of the array and adds each to stack.
				secondHalf.push(s2.charAt(i));
			}
			for (int i = 0; i < (s2.length() / 2); i++) { //checks through both stacks to find if each letter matches.
				if (firstHalf.pop() == secondHalf.pop()) {
					result = true;
					continue;
				} else {//end loop if anything turns out false to preserve result.
					result = false;
					break;
				}
			} 
		}
		return result;
		
		
	}

}
