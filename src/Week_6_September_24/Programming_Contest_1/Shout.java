package Week_6_September_24.Programming_Contest_1;

import java.util.Scanner;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 24, 2018
 *
 */
public class Shout {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in =  new Scanner(System.in);
		
		String a;
		try {
			a = in.nextLine();
		} finally {
			in.close();
		}
		
		System.out.println(a.toUpperCase());
	}

}
