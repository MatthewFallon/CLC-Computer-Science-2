package Week_6_September_24.Programming_Contest_1; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 24, 2018
 *
 */
public class LunchBag {

	
	public static void main(String[] args) {
		ArrayBag<String> lunch = new ArrayBag<>();
		
		lunch.add("Sandwich");
		lunch.add("Chips");
		
		System.out.println(lunch.remove());
		System.out.println(lunch.remove());
	}
}
