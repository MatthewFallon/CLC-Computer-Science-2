package Week_6_September_24.Programming_Contest_1;

import java.util.Scanner;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 24, 2018
 *
 */
public class EvenorOdd {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		try {
			
			int n = in.nextInt();

			if (n % 2 == 0) {
				System.out.println("even");
			} else {
				System.out.println("odd");
			} 
		} finally {
			in.close();
		}
		
	}

}
