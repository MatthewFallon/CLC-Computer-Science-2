package Week_6_September_24.Programming_Contest_1; /**
 * 
 */

/** Evaluates algorithms for efficiency at small and large values of n. 
 * Program 4 pg. 150 #2.
 * 
 * @author Matthew Fallon
 * @since Sep 19, 2018
 *
 */
public class AlgAnalysis {

	/** Main runner class that checks timings for various algorithms and finds the faster one in each case.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		long start, end, timea, timeb;
		
		int n = 0;
		boolean done = false;
		while (!done) {
			start = timer();
			LoopA(n);
			end = timer();
			timea = end - start;
			System.out.println("Time for loop a for n = " + n + " was: " + (timea) + " nanoseconds.");
			start = timer();
			LoopB(n);
			end = timer();
			timeb = end - start;
			System.out.println("Time for loop b for n = " + n + " was: " + (timeb) + " nanoseconds.");
			System.out.println();
			
			if (timea < timeb) {
				System.out.println("result is " + n);
				break;
			}
			else {
				n += 10;
			}
		}
		
		
		
	}
	
	private static long timer() {
		return System.nanoTime();
	}
	
	private static int LoopA(int n) {
		int sum = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= 10000; j++) {
				sum = sum + j;
			}
		}
		return sum;
	}
	
	private static int LoopB(int n) {
		int sum = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				sum = sum + j;
			}
		}
		return sum;
	}

}
