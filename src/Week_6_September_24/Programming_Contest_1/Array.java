package Week_6_September_24.Programming_Contest_1; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 24, 2018
 *
 */
public class Array {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] num = new int[1000];
		for (int i = 1; i <= 1000; i++) {
			num[i - 1] = (-1 * i);
		}
		for (int i : num) {
			System.out.println(i);
		}
	}

}
