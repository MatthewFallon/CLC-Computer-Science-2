package Week_6_September_24.Programming_Contest_1; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 24, 2018
 *
 */
public class Triangle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
