package Week_9_October_15.Lab_J4_Iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/** Tester class for observing iterators.
 * @author Matthew Fallon
 * @since 10-15-18
 */

public class IteratorTest {

    public static void main(String[] args) {
        ArrayList<String> nameList0 = new ArrayList<>();

        nameList0.add("Jamie");
        nameList0.add("Joey");
        nameList0.add("Rachel");

        System.out.println(nameList0);

        Iterator<String> it0 = nameList0.iterator();

        while (it0.hasNext()) {
            System.out.println(it0.next());
        }


        for (String each :
                nameList0) {
            System.out.println(each);
        }

        LinkedList<Name> nameList1 = new LinkedList<>();

        nameList1.add(new Name("Mr.", "Reed"));
        nameList1.add(new Name("Mrs.", "Reed"));

        System.out.println(nameList1);

        Iterator<Name> it1 = nameList1.iterator();

        while (it1.hasNext()) {
            System.out.println(it1.next());
        }



        LinkedList<String> nameList2 = new LinkedList<>();
        nameList2.add("Andy");
        nameList2.add("Brittany");
        nameList2.add("Chris");
        System.out.println(nameList2);

        Iterator<String> it2 = nameList2.iterator();

        it2.next();
        it2.next();
        it2.remove();
        it2.next();
        System.out.println(nameList2);

        nameList2.add("Brittany");
        System.out.println(nameList2);

        it2 = nameList2.iterator();
        it2.hasNext();
//        it2.remove();



    }
}
