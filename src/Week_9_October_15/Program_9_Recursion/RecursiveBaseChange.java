package Week_9_October_15.Program_9_Recursion;

/** Allows you to change a given integer into a given base and demonstrates that with a main class.
 *
 * @author Matthew Fallon
 * @since 10-16-18
 *
 */

public class RecursiveBaseChange {

    /**
     *
     * @param args unused.
     */
    public static void main(String[] args) {
        int test = 10;
        for (int i = 0; i < test; i++) {
            int temp = baseDecimal(i);
            System.out.println(temp);
            assert temp == i;
        }

        for (int i = 0; i < test; i++) {
            System.out.println(baseChange(i,2));
        }

    }

    private static int baseDecimal (int num) {
        int firstNum = num % 10;
        if (num / 10 != 0)
            return firstNum + (10 * baseDecimal(num / 10));
        else
            return firstNum;
    }

    private static int baseChange (int num, int base) {
        int firstNum = num % base;
        if (num / base > 0)
            return firstNum + (10 * baseChange(num / base,base));
        else
            return firstNum;
    }

}
