package Week_9_October_15.Lab_J4_ListIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/** Question 6 from the second lab of the day.
 *
 * @author Matthew Fallon
 * @since 10-15-18
 */

public class Question6 {

    public static void main(String[] args) {
        List<String> nameList0 = new ArrayList<>();
        nameList0.add("Jen");
        nameList0.add("Jim");
        nameList0.add("Josh");
        System.out.println(nameList0);

        ListIterator<String> it0 = nameList0.listIterator();

        it0.next();
        it0.next();
        it0.next();
        it0.set("John");

    }
}
