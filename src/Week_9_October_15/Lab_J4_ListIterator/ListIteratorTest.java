package Week_9_October_15.Lab_J4_ListIterator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class ListIteratorTest {

    static public void main(String[] args) throws FileNotFoundException {
        File file = new File("src/Week_9_October_15/Lab_J4_ListIterator/name.txt");
        BufferedReader in = new BufferedReader(new FileReader(file));
        Scanner input = new Scanner(in);

        ArrayList<String> result = new ArrayList<>();


        while (input.hasNextLine()) {
            String temp = input.nextLine();
            result.add(temp.substring(0,temp.indexOf(" ")));
        }

        System.out.println(result);

        for (String each :
                result) {
            System.out.println(each);
        }

        System.out.println();
        result.sort(Comparator.naturalOrder());

        for (String each :
                result) {
            System.out.println(each);
        }


        input.close();

    }
}
