package Week_16_December_3.Homework_25_3_BinaryTree;

import java.util.Iterator;

import Week_16_December_3.Homework_25_3_BinaryTree.TreePackage.BinaryTree;

/**Constructs a basic binary tree and performs an inorder traversal.
 * 
 * @author Matthew Fallon
 * Im adding this in a comment to make a change
 *
 * @since 11/28/2018
 *
 */

public class BasicBinaryTree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Name rootA = new Name("Mr.", "A");
		Name childB = new Name("Mr.", "B");
		Name childC = new Name("Mr.", "C");
		
		BinaryTree<Name> abc = new BinaryTree<>();
		BinaryTree<Name> left = new BinaryTree<>(childB);
		BinaryTree<Name> right = new BinaryTree<>(childC);
		
		abc.setTree(rootA, left, right);
		
		Iterator<Name> iterate = abc.getInorderIterator();
		
		while (iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		
		
		

		
	}

}
