package Week_10_October_22.Homework_9_13_4_RecursiveValue;

/** Calls a recursive function to add all integers from 1 to n.
 *
 * @author Matthew Fallon
 * @since 10-24-18
 *
 */

public class Main {

	public static void main(String[] args) {
		for ( int i = 1; i < 10; i++) {
			System.out.print(i + ": ");
			System.out.println(recursiveSum(i));
		}
		
	}

	/** Recursively calls itself to sum all numbers below n with itself.
	 *
	 * @param n number above 0 to be added with integers below.
	 * @return added up sum.
	 */

	public static int recursiveSum(int n) {
		if (n > 0) {
			n += recursiveSum(n - 1);
		}
		return n;
	}

}
