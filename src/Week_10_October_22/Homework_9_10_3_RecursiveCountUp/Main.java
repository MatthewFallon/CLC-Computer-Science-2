package Week_10_October_22.Homework_9_10_3_RecursiveCountUp;

/** Program with a void method to print out a count up to n.
 * 
 * @author Matthew Fallon
 * @since 10-18-18
 */

public class Main {

	public static void main(String[] args) {
		countUp(10);

	}
	
	/** Recursive function that calls repeatedly before printing out from each call to count up to n.
	 * 
	 * @param n number to count up to.
	 */
	public static void countUp(int n) {
		if(n > 1) {
			countUp(n-1);
		}
		System.out.println(n);
	}

}
