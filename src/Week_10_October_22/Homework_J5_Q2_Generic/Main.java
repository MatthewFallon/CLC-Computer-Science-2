package Week_10_October_22.Homework_J5_Q2_Generic;

/** Uses a generic swap method to exchange to entries in a Name[].
 * @author Matthew Fallon
 * @since 10/24/2018
 */
public class Main {

    public static void main(String[] args) {
        Name[] arr = new Name[] {
                new Name("Mikaela", "Hardin"),
                new Name("Vikki", "Worthington"),
                new Name("Alena", "Dalby"),
                new Name("Manav", "Mcphee"),
                new Name("Maggie", "Flower"),
        };

        for (Name each :
                arr) {
            System.out.println(each);
        }
        System.out.println();

        arr = swap(arr, 1, 4);

        for (Name each :
                arr) {
            System.out.println(each);
        }

    }

    /** Swaps two entries in an array of a generic type.
     *
     * @param in Array of generic type
     * @param first Index of first object to swap.
     * @param second Index of second object to swap.
     * @param <T> Generic type T of the objects in an array.
     * @return Array with same type of objects with two objects swapped.
     */
    private static <T> T[] swap(T[] in, int first, int second) {
        T temp = in[first];
        in[first] = in[second];
        in[second] = temp;
        return in;
    }
}
