package Week_10_October_22.Homework_10_10_ListRunners;

import java.util.ArrayList;
import java.util.List;

/** Prints out a list of runners.
 * @author Matthew Fallon
 * @since 10/24/2018
 */
public class RoadRace {

    public static void main(String[] args) {
        recordWinners();
    } //end main

    /** creates list of runners and prints out.
     *
     */
    public static void recordWinners() {
        List< Integer > runnerList = new ArrayList< >();
        //runnerList has only methods in Listinterface.

        runnerList.add(16); //Winner
        runnerList.add( 4); //Second place
        runnerList.add(33); //Third place
        runnerList.add(27); //Fourth place
        displayList(runnerList);
    }//end recordWinners

    /** prints a given list with formatting for stating positions.
     *
     * @param list list to print.
     */
    public static void displayList(List<Integer> list) {
        int numberOfEntries = list.size();
        System.out.println("The list contains " + numberOfEntries + " entries, as follows:");

        for (int position = 0; position < numberOfEntries; position++) {
            System.out.println(list.get(position) +
                                " is entry " + (position + 1));
        }

        System.out.println();
    }//end displayList
}
