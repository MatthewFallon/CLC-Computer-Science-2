package Week_10_October_22.Homework_J4_Iterator;

import java.util.ArrayList;
import java.util.Iterator;

/** Tests out various operations using iterators.
 *
 * @author Matthew Fallon
 * @since 10/24/2018
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<String> nameList = new ArrayList<>();

        nameList.add("Jamie");
        nameList.add("Joey");
        nameList.add("Rachel");

        Iterator<String> nameIterator = nameList.iterator();
        nameIterator.next();
        nameIterator.next();
        nameIterator.remove();
        System.out.println(nameIterator.hasNext());
        System.out.println(nameIterator.next());

        nameList.add(1,"joey");
       nameIterator = nameList.iterator();

        for (int i = 0; i < 2; i++) {
            nameIterator.next();
        }
        System.out.println(nameIterator.next());

        nameIterator = nameList.iterator();

        int count = 0;
        while (nameIterator.hasNext()) {
            count++;
            if (count % 2 == 0) {
                System.out.println(nameIterator.next());
            }
            else {
                nameIterator.next();
            }
        }

        nameIterator = nameList.iterator();

        while (nameIterator.hasNext()) {
            nameIterator.next();
            nameIterator.remove();
        }
        System.out.println(nameList);
    }
}
