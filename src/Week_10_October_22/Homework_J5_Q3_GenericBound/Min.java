package Week_10_October_22.Homework_J5_Q3_GenericBound;

/** Finds the minimum of two objects and tests the method.
 * @author Matthew Fallon
 * @since 10/24/2018
 */
public final class Min {

    /** invokes compareTo to find the smaller of two generic objects.
     *
     * @param x First to compare.
     * @param y Second to compare.
     * @param <T> Generic variable.
     * @return Return of the smaller generic object.
     */

    public static <T extends Comparable<T>> T smallerOf(T x, T y) {
        if (x.compareTo(y) < 0) {
            return x;
        }
        else {
            return y;
        }
    }//end smallerOf

    /**
     *
     * @param args unused
     */
    public static void main(String[] args) {
        int n = smallerOf( 3, 5 );
        double x = smallerOf( 3.5, 5.6 );
        String str0 = smallerOf( "Mr.", "Mrs." );
        Name n0 = smallerOf( new Name( "Mr.", "Reed" ), new Name( "Mrs.", "Reed" ) );

        System.out.printf("%d, %f, %s, %s", n, x, str0, n0);
    }
}//end Min
