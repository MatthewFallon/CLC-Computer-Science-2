package Week_10_October_22.Program_10_Lists;

/**
 * @author Matthew Fallon
 * @since 10/24/2018
 */
public class Client {

    public static void main(String[] args) {

        ListInterface<Student> studentList = new AList<>();

        Student student1 = new Student(new Name("Matthew", "Fallon"), "000001");
        Student student2 = new Student(new Name("Mr.", "Reed"), "000002");
        Student student3 = new Student(new Name("John", "Doe"), "000003");

        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);


        for (int i = 1; i <= studentList.getLength(); i++) {
            System.out.println(studentList.getEntry(i).getName().getLast());
        }


        Student s0 = studentList.getEntry(1); //create a working String variable with the variable from the first slot in the swap.
        s0 = studentList.replace(3, s0); //Sets the variable in the second slot to the first slot's variable and sets the variable.
        studentList.replace(1, s0);

        for (int i = 1; i <= studentList.getLength(); i++) {
            System.out.println(studentList.getEntry(i).getName().getLast());
        }


    }


}
