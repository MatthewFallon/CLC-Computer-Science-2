package Week_5_September_17.Lab_7_3_Queue;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 19, 2018
 *
 */
public class LabQueueRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Queue<String> myQueue = new ArrayDeque<>();
		
		myQueue.add("Jada");
		myQueue.add("Jess");
		myQueue.add("Jazmin");
		myQueue.add("Jorge");
		String name = myQueue.remove();
		myQueue.add(name);
		myQueue.add(myQueue.peek());
		name = myQueue.remove();
		
		System.out.println(myQueue);
		
		
	}

}
