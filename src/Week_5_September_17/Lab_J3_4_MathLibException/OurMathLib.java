package Week_5_September_17.Lab_J3_4_MathLibException;

/**
   A class of static methods to perform various mathematical
   computations, including the square root.
   @author Frank M. Carrano
   @author Timothy M. Henry
   @author Matthew Fallon
   @version 5.0
*/
public class OurMathLib
{
   /** Computes the square root of a nonnegative real number.
       @param value  A real value whose square root is desired.
       @return  The square root of the given value.
       @throws  SquareRootException if value < 0. */
   public static double squareRoot(double value) //throws SquareRootException
   {
	  try { 
	      if (value < 0)
	         throw new SquareRootException();
	      else 
	         return Math.sqrt(value);
	  } catch (SquareRootException e) {
		System.out.print("not ");
		System.err.println(e.getMessage() + " " + value);
		return value;
	  }
   } // end squareRoot
   
   // < Other methods not relevant to this discussion are here. >
   
} // end OurMath
