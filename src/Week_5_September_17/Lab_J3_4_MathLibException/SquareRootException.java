package Week_5_September_17.Lab_J3_4_MathLibException; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Sep 19, 2018
 *
 */
@SuppressWarnings("serial")
public class SquareRootException extends RuntimeException {
	
	/**
	 * basic constructor throws default error
	 * 
	 */
	public SquareRootException() {
		super("Attempted square root of a negative number.");
	} //end default constructor
	
	public SquareRootException(String message) {
		super(message);
	} // end constructor
	
}//end class
