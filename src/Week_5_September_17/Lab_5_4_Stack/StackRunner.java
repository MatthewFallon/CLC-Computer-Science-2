package Week_5_September_17.Lab_5_4_Stack;

import java.util.Stack;

/**Creates and implements a stack for manipulating names as strings.
 * 
 * @author Matthew Fallon
 * @since 9-17-18
 */
public class StackRunner {

	/** Main runner class.
	 * @param args
	 */
	public static void main(String[] args) {
		
		//StackInterface<String> stringStack = new ourStack<>();
		
		Stack<String> stringStack = new Stack<>();
		Stack<String> nameStack = new Stack<>();
		
		stringStack.push("Jim");
		stringStack.push("Jazmin");
		stringStack.pop();
		stringStack.push("Sophia");
		stringStack.push("Tia");
		stringStack.pop();
		
		//swaps strings to nameStack.
		for (String each : stringStack) {
			nameStack.push(each);//each is not popped from stringStack
		}
		stringStack.clear();//clears out the unpopped stringStack
		
		System.out.println("String Stack = " + stringStack);
		System.out.println("Name Stack = " + nameStack);
		
		//print out
		for (String each : nameStack) {
			System.out.println(each);
		}
		nameStack.clear();
		
		//This for each loop doesn't run because the stack should be empty.
		for (String each : stringStack) {
			System.out.println(each);
			
		}
		
		System.out.println("String Stack = " + stringStack);
		System.out.println("Name Stack = " + nameStack);
	}

}
