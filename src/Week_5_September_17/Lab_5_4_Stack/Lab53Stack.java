package Week_5_September_17.Lab_5_4_Stack;

import java.util.Stack;

/**plays with stacks.
 * 
 * @author Matthew Fallon
 * @since 9-17-18
 *
 */
public class Lab53Stack {

	/** Main class runner
	 * @param args
	 */
	public static void main(String[] args) {

		Stack<String> s = new Stack<>();
		Stack<String> t = new Stack<>();
		
		String a = "Mr. Reed";
		String b = "Matthew Fallon";
		String c = "Drake";
		String d = "Josh";
		
		s.push(a);
		s.push(b);
		s.push(c);
		t.push(d);
		t.push(s.pop());
		t.push(s.peek());
		s.push(t.pop());
		t.pop();
		
		System.out.println("s = " + s);//printout s
		System.out.println("t = " + t);//printout t
		
	}

}
