package Week_5_September_17.Program_3_LinkedBag; /**
 * 
 */

/** pg. 125 #1 in the textbook for program 3. 
 * @author Matthew Fallon
 * @since Sep 24, 2018
 *
 */
public class Prog3LinkedBagRunner {

	/** Tests a linked bag and prints out.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		Integer[] intArr = new Integer[] {
				1,
				2,
				3,
				4,
				5
		};
		
		LinkedBag<Integer> b1 = new LinkedBag<>(intArr);
		System.out.println(b1);
		
		String[] stringArr = new String[] {
				"student",
				"awesome",
				"an",
				"is",
				"Matthew"
				
		};
		
		LinkedBag<String> b0 = new LinkedBag<>(stringArr);
		System.out.println(b0);
	
		
		
		
	}	

}
