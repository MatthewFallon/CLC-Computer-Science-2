package Week_8_October_8.Lab_10_8_SantaList;

import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 10, 2018
 *
 */
public class SantaList {

	/**
	 * @param args unused
	 */
	public static void main(String[] args) {
		 
		NaughtyChild naughtyC0 = new NaughtyChild(new Name("Mr.", "Grinch"));
		NaughtyChild naughtyC1 = new NaughtyChild(new Name("Mr.", "Bad"));
		
		ArrayList<NaughtyChild> naughtyList = new ArrayList<>();
		
		naughtyList.add(naughtyC0);
		naughtyList.add(naughtyC1);
		
		ArrayList<String> gift0 = new ArrayList<>();
		gift0.add("money ");
		gift0.add("candy ");
		gift0.add("new Computer");
		
		ArrayList<String> gift1 = new ArrayList<>();
		gift1.add("flowers ");
		gift1.add("candles ");
		gift1.add("healthyFood ");
		gift1.add("chocolateTreats");
		
		ArrayList<String> gift2 = new ArrayList<>();
		gift2.add("ramen ");
		gift2.add("RAM ");
		gift2.add("RAM ");
		gift2.add("moreRAM");
		
		
		NiceChild niceC0 = new NiceChild(new Name("Mr.", "Reed"), gift0);
		NiceChild niceC1 = new NiceChild(new Name("Mother", "Dear"), gift1);
		NiceChild niceC2 = new NiceChild(new Name("Matthew", "Fallon"), gift2);
		
		
		ArrayList<NiceChild> niceList = new ArrayList<>();
		
		niceList.add(niceC0);
		niceList.add(niceC1);
		niceList.add(niceC2);
		
		
		System.out.println(naughtyList);
		System.out.println(niceList);
		
		
		
	}

}
