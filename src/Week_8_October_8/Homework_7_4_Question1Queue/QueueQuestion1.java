package Week_8_October_8.Homework_7_4_Question1Queue;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 
 */

/** Demonstrates a queue using the statements from p. 209
 * @author Matthew Fallon
 * @since Oct 3, 2018
 *
 */
public class QueueQuestion1 {

	/** Main runner class.
	 * @param args
	 */
	public static void main(String[] args) {
		Queue< String > q0 = new ArrayDeque< >();  //  java.util.Queue interface and java.util.ArrayDeque implementation.
		
		q0.add("Jada");
		q0.add("Jess");
		q0.add("Jazmin");
		q0.add("Jorge");
		String name = q0.remove();
		q0.add(name);
		q0.add(q0.peek());
		name = q0.remove();
		
		for (int i = 0; i < 2; i++) { //Queue can already iterate through queue while preserving contents.
			System.out.println();
			for (String each : q0) {
				System.out.println(each);
			}
		}
		
		for (int h = 0; h < 2; h++) {
			System.out.println();
			for (int i = 0; i < q0.size(); i++) {
				String workingName = q0.remove();
				System.out.println(workingName);
				q0.add(workingName);
			}
		}
			
	}

}
