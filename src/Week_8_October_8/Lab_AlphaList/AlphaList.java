package Week_8_October_8.Lab_AlphaList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Matthew Fallon
 * @since Oct 8, 2018
 *
 *
 */
public class AlphaList {


	public static void main(String[] args) {
		
		List<String> workingList = new ArrayList<>();
		
		workingList.add("Amy");
		workingList.add("Elias");
		workingList.add("Bob");
		workingList.add("Drew");
		
		List<String> workingList1 = new ArrayList<>(workingList);

		
		String s0 = workingList1.get(1); //create a working String variable with the variable from the first slot in the swap.
		s0 = workingList1.set(2, s0); //Sets the variable in the second slot to the first slot's variable and sets the variable.
		workingList1.set(1, s0);
		
		s0 = workingList1.get(2);
		s0 = workingList1.set(3, s0);
		workingList1.set(2, s0);
		
		System.out.println(workingList1);//Prints swapped list.
		

		
		workingList.sort(Comparator.naturalOrder()); //Sorts working list using the comparator library's natural order comparator.
		//Sorts list alphabetically.
		//sort(workingList);
		System.out.println(workingList);//prints sorted list.
		
	}

}
