package Week_8_October_8.Lab_NameList;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 8, 2018
 *
 */
public class NameList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Name amy = new Name("Amy", "Smith");
		Name tina = new Name("Tina", "Drexel");
		Name robert = new Name("Robert", "Jones");
		
		List<Name> nameList = new ArrayList<Name>();
		
		nameList.add(amy);
		nameList.add(tina);
		nameList.add(robert);
		
		System.out.println(nameList);

	}

}
