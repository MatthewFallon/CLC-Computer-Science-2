package Week_8_October_8.Homework_7_14_Question4Deque;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 
 */

/** Page 222 Question #4 Tests out the Dequeue function.
 * @author Matthew Fallon
 * @since Oct 10, 2018
 *
 */
public class Dequeue {

	/**
	 * @param args unused
	 */
	public static void main(String[] args) {
		Deque< String > q0 = new ArrayDeque< >();  //  java.util.Deque interface and java.util.ArrayDeque implementation.
		q0.addFirst("Jim");
		q0.addLast("Jess");
		q0.addFirst("Jill");
		q0.addLast("Jane");
		String n0 = q0.getFirst();
		q0.addLast(n0);
		q0.removeFirst();
		q0.addFirst(q0.removeLast());
		
		System.out.println(q0);
		
		for (int i = 0; i < q0.size(); i++) {
			String temp = q0.removeFirst();
			System.out.println(temp);
			q0.addLast(temp);
		}
		
		System.out.println(q0);
		System.out.println("Dequeue is the same.");
	}

}
