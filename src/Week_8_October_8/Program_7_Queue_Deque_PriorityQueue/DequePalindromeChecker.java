package Week_8_October_8.Program_7_Queue_Deque_PriorityQueue;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 14, 2018
 *
 */
public class DequePalindromeChecker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String[] sample = new String[] {
				"A but tuba.",
				"A car a man a maraca.",
				"A dog a plan a canal: pagoda.",
				"A dog! A panic in a pagoda!",
				"A lad named E. Mandala",
				"A man a plan a canal: Panama.",
				"A man a plan a cat a ham a yak a yam a hat a canal-Panama!",
				"A new order began a more Roman age bred Rowena.",
				"A nut for a jar of tuna.",
				"A Santa at Nasa.",
				"A Santa dog lived as a devil God at NASA.",
				"A slut nixes sex in Tulsa.",
				"A tin mug for a jar of gum Nita.",
				"A Toyota! Race fast safe car! A Toyota!",
				"A Toyota�s a Toyota.",
				"Able was I ere I saw Elba.",
				"Acrobats stab orca.",
				"Aerate pet area.",
				"Ah Satan sees Natasha!",

				"feather",
				"vessel",
				"applaud",
				"pray",
				"jury",
				"fascinate",
				"progress",
				"guess",
				"quiet",
				"agenda",
				"constellation",
				"crossing",
				"bold",
				"calendar",
				"form",
				"bar",
				"continental",
				"plant",
				"basic",
				"trap",
				"gas",
				"occupy",
				"understand",
				"rank",
				"beach",
		} ;
		
		

		for (String each : sample) {
			if (isPalindrome(each)) {
				System.out.println("\"" + each + "\" is a palindrome.");
			}
			else {
				System.out.println("\"" + each + "\" isn't a palindrome.");
			}
		}

	}
	
	public static boolean isPalindrome(String input) {
		
		Deque<Character> queue = new LinkedBlockingDeque<>();
		
		input = input.toLowerCase().trim().replaceAll("[^a-z]", "");

		for (Character each :
				input.toCharArray()) {
			queue.push(each);
		}

		int size = queue.size();
		for (int i = 0; i < size / 2; i++) {
			if (!(queue.removeFirst().equals(queue.removeLast()))) {
				return false;
			}
		}


		
		
		return true;
	}

}
