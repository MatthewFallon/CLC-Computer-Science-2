package Week_8_October_8.Homework_7_19_Question5PriorityQueue;

import java.util.PriorityQueue;
/**
 * 
 */

/** Page 227 Question #5 
 * @author Matthew Fallon
 * @since Oct 10, 2018
 *
 */
public class PriorityQueueHW {

	/**
	 * @param args unused
	 */
	public static void main(String[] args) {
		PriorityQueue<String> q0 = new PriorityQueue<>();  //  java.util.PriorityQueue
		q0.add("Jane");
		q0.add("Jim");
		q0.add("Jill");
		String n0 = q0.remove();
		q0.add(n0);
		q0.add("Jess");
		
		System.out.println(q0);
		
		String[] arr = new String[q0.size()];

		int size = q0.size();
		
		for (int i = 0; i < size; i++) {
			String temp = q0.remove();
			System.out.println(temp);
			arr[i] = temp;
		}
		
		
		for (int i = 0; i < arr.length; i++) {
			q0.add(arr[i]);
		}
		
		
		System.out.println(q0);
		System.out.println("Priority Queue order is preserved.");
		
		
	}

}
