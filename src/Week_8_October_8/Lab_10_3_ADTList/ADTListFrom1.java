package Week_8_October_8.Lab_10_3_ADTList;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 8, 2018
 *
 */
public class ADTListFrom1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> myList = new ArrayList<>();
		myList.add("a");
		myList.add("b");
		myList.add("c");
		myList.add(1, "d");
		myList.add(0, "e");
		myList.remove(2);
		
		System.out.println(myList);
		

	}

}
