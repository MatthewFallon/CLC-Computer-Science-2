package Week_8_October_8.Lab_10_3_ADTList;

import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 8, 2018
 *
 */
public class ADTList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<String> myList = new ArrayList<>();
		myList.add("a");
		myList.add("b");
		myList.add("c");
		myList.add(2, "d");
		myList.add(1, "e");
		myList.remove(3);
		
		System.out.println(myList);
		

	}

}
