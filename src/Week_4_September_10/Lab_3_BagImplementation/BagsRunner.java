package Week_4_September_10.Lab_3_BagImplementation;


/**class for the main method of the project
 * @author mfallon
 * @since 9-10-18
 */
public class BagsRunner {

	/**Creates a series of bags to test the implementation of each.
	 * @param args
	 */
	public static void main(String[] args) {
		
		//ArrayBag tests
		BagInterface<String> aBag = new ArrayBag<String>(3);
		
		aBag.add("A");
		aBag.add("B");
		aBag.add("C");
		
		System.out.println(aBag.remove());
		System.out.println(aBag.remove());
		System.out.println(aBag.remove());
		
		
		System.out.println();
		System.out.println();
		//LinkedBag tests
		
		aBag = new LinkedBag<String>();
		
		aBag.add("A");
		aBag.add("B");
		aBag.add("C");
		
		System.out.println(aBag.remove());
		System.out.println(aBag.remove());
		System.out.println(aBag.remove());
		
		
		System.out.println();
		System.out.println();
		
		
		//LinkedBag tests
		
		BagInterface<Name> nameBag = new LinkedBag<Name>();
				
		nameBag.add(new Name("Matthew", "Fallon"));
		nameBag.add(new Name("Mr.", "Reed"));
		nameBag.add(new Name("John", "Wicke"));
		
		System.out.println(nameBag.remove());
		System.out.println(nameBag.remove());
		System.out.println(nameBag.remove());
		
		
		System.out.println();
		System.out.println();
		
		

		
		
	}

}
