While aBag's current size isn't equal to its capacity

    Ask for a new string from the user
    Add new string to aBag

While aBag isn't empty

    Remove an item from aBag
    Have that item print to the console

Print out the frequency of the string "Hello" in the bag