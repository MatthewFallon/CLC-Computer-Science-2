package Week_4_September_10.Program_1_7_FullBag;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * The main runner class for testing and manipulating an arrayBag.
 *
 * @author Matthew Fallon
 * @since 9-11-18
 */
public class FullBagRunner {

    /**
     * Creates an arrayBag and adds strings from a specified text file until it's full before printing all the items and the frequency of the String "Hello"
     *
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        Scanner in = new Scanner(new File("src/Week_4_September_10/Program_1_7_FullBag/UserSuppliedStrings.txt"));//scanner reads from specified File input.

        final int CAPACITY = (int) (Math.random() * 30);
        BagInterface<String> aBag = new ArrayBag<String>(CAPACITY);

        while (aBag.getCurrentSize() < CAPACITY) {
            aBag.add(in.nextLine().trim());//reads each name from the given file
        }

        int f = aBag.getFrequencyOf("Hello");// grabs frequency of "Hello" Strings before dumping the bag out.

        while (!(aBag.isEmpty())) {//removes and prints all items.
            System.out.println(aBag.remove());
        }

        System.out.println(f);//prints out predetermined frequency.

        in.close();

    }

}
