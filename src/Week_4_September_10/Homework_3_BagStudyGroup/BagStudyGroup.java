package Week_4_September_10.Homework_3_BagStudyGroup; /**
 * 
 */

/**Main running class for creating a bag that represents a study group of CollegeStudents.
 * @author Matthew Fallon
 * @since 9-12-18
 *
 */
public class BagStudyGroup {

	/** Main method creates a bag for college students and add and removes them to demo it.
	 * @param args
	 */
	public static void main(String[] args) {
		
		//bag for 3 students
		BagInterface<Student> studyGroup = new ArrayBag<>(3);
		
		//creates each college student while adding them to bag.
		studyGroup.add(new CollegeStudent(new Name("Matthew", "Fallon"), "000001", 2020, "Bachelors in Computer Science"));
		studyGroup.add(new CollegeStudent(new Name("Mr.", "Reed"), "000002", 1976, "Bachelors in Math"));
		studyGroup.add(new CollegeStudent(new Name("John", "Doe"), "000003", 2022, "Bachelors in Communications"));
		
		//removes and prints all members of study group.
		for (int i = 0; i < studyGroup.getCurrentSize(); i++ ) {
			System.out.println(studyGroup.remove());
			System.out.println(studyGroup.remove());
			System.out.println(studyGroup.remove());
		}

	}

}
