package Week_7_October_1.Lab_9_Eclipse; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 1, 2018
 *
 */
public class HelloWorld {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Hello World!");
		
		System.out.println("PI       = " + Math.PI);
		
		System.out.println("root (2) = " + Math.sqrt(2.0)); //

	}

}
