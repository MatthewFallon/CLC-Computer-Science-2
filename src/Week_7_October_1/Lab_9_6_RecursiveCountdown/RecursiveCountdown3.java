package Week_7_October_1.Lab_9_6_RecursiveCountdown; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 1, 2018
 *
 */
public class RecursiveCountdown3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

		countdown3(10);
	}
	
	public static void countdown3(int integer) {
		System.out.print(integer + " ");
		
		if (integer > 1) {
			countdown3(integer - 1);
		} 
	}

}
