package Week_7_October_1.Lab_9_6_RecursiveCountdown; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 1, 2018
 *
 */
public class RecursiveCountdown2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		countdown2(10);
	}

	public static void countdown2(int integer) {
		if (integer > 0) {
			System.out.print(integer + " ");
			countdown2(integer - 1);
		} 
	}

}
