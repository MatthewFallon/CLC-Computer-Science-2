package Week_7_October_1.Lab_9_6_RecursiveCountdown; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 1, 2018
 *
 */
public class RecursiveCountdown {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

		countdown(10);
	}
	
	public static void countdown(int integer) {
		if (integer == 1) {
			System.out.print(integer + " ");
		} else {
			System.out.print(integer + " ");
			countdown(integer - 1);
		}
	}

}
