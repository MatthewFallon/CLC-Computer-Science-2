package Week_7_October_1.Lab_9_15_RecursiveDisplayArray; /**
 * 
 */

/**
 * @author Matthew Fallon
 * @since Oct 3, 2018
 *
 */
public class Array3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] array = new int[] {
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				34,
				92
		};
		
		displayArray(array, 2, 6);
	}
	
	public static void displayArray(int[] array, int first, int last) {
		if (first == last) {
			System.out.print(array[first] + " ");
		}
		else {
			int mid = (first + last) / 2;
			displayArray(array, first, mid);
			displayArray(array, mid + 1, last);
		}
	}

}
