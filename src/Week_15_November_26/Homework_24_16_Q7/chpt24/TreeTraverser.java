package Week_15_November_26.Homework_24_16_Q7.chpt24;

import Week_15_November_26.Homework_24_16_Q7.chpt24.TreePackage.BinaryTree;
import Week_15_November_26.Homework_24_16_Q7.chpt24.TreePackage.BinaryTreeInterface;

import java.util.Iterator;

// @author Frank M. Carrano, Timothy M. Henry
// @author Matthew Fallon
// @version 5.0

// Represent each leaf as a one-node tree

public class TreeTraverser {
	public static void main(String[] args) {
		BinaryTreeInterface<String> dTree = new BinaryTree<>();
		dTree.setTree("James", null, null);

		BinaryTreeInterface<String> fTree = new BinaryTree<>();
		fTree.setTree("Robert", null, null);

		BinaryTreeInterface<String> gTree = new BinaryTree<>();
		gTree.setTree("Carole", null, null);

		BinaryTreeInterface<String> hTree = new BinaryTree<>();
		hTree.setTree("H", null, null);

		BinaryTreeInterface<String> emptyTree = new BinaryTree<>();

		// Form larger subtrees
		BinaryTreeInterface<String> eTree = new BinaryTree<>();
		eTree.setTree("Mary", null, null); // Subtree rooted at E

		BinaryTreeInterface<String> bTree = new BinaryTree<>();
		bTree.setTree("John", dTree, eTree); // Subtree rooted at B

		BinaryTreeInterface<String> cTree = new BinaryTree<>();
		cTree.setTree("Jennifer", fTree, gTree); // Subtree rooted at C

		BinaryTreeInterface<String> aTree = new BinaryTree<>();
		aTree.setTree("Jared", bTree, cTree); // Desired tree rooted at A

		// Display root, height, number of nodes
		System.out.println("Root of tree contains " + aTree.getRootData());
		System.out.println("Height of tree is " + aTree.getHeight());
		System.out.println("Tree has " + aTree.getNumberOfNodes() + " nodes");

		// Display nodes in preorder
		System.out.println("A preorder traversal visits nodes in this order:");
		Iterator<String> preorder = aTree.getPreorderIterator();
		while (preorder.hasNext())
			System.out.print(preorder.next() + " ");
		System.out.println();

		// Display nodes in inorder
		System.out.println("A inorder traversal visits nodes in this order:");
		Iterator<String> inorder = aTree.getInorderIterator();
		while (inorder.hasNext())
			System.out.print(inorder.next() + " ");
		System.out.println();

		// Display nodes in postorder
		System.out.println("A postorder traversal visits nodes in this order:");
		Iterator<String> postorder = aTree.getPostorderIterator();
		while (postorder.hasNext())
			System.out.print(postorder.next() + " ");
		System.out.println();

		// Display nodes in levelorder
		System.out.println("A levelorder traversal visits nodes in this order:");
		Iterator<String> levelorder = aTree.getLevelOrderIterator();
		while (levelorder.hasNext())
			System.out.print(levelorder.next() + " ");
		System.out.println();

	}
}
