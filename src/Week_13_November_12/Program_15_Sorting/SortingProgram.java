package Week_13_November_12.Program_15_Sorting;


import Week_6_September_24.Programming_Contest_1.Array;

import java.util.Arrays;

/** Program 15 Sorting p480 #10
 *
 * This sort uses a conversion to an int form of the binary base, and has a maximum range
 * of numbers between 0 - 1000.
 * 
 * @author Matthew Fallon
 * @since November 5, 2018
 *
 */

public class SortingProgram {


	/** Main runner class. Sends a random length array of random integers
	 * from 0 to 1000 at the binary search algorithm.
	 *
	 * @param args unused.
	 */
	public static void main(String[] args) {


		final int RAND = (int) (Math.random() * 30);
		int[] a = new int[RAND];

		for (int i = 0; i < RAND; i++) {
			a[i] = (int) (Math.random() * 1000);
		}

		System.out.println("Array before sort:");
		System.out.println(Arrays.toString(a));
		System.out.println("Array after sort:");
		System.out.println(Arrays.toString(binaryRadixSort(a)));

	}


	/** Takes in an integer array, converts it to binary form and performs
	 *  a radix sort on the numbers before converting them back to decimal form.
	 *
	 *  O(n) complexity.
	 * @param a The array to be sorted.
	 * @return The sorted array.
	 */
	private static int[] binaryRadixSort(int[] a) {
		int[][] buckets = new int[2][a.length];


		for (int i = 0; i < a.length; i++) { //change array to binary
			buckets[0][i] = decimalToBase(a[i], 2);
		}


		for (int i = 0; i < (4 * 8); i++) { //runs for each bit in an integer.
			int bucket0Size = 0;
			int bucket1Size = 0;
			for (int j = 0; j < buckets[0].length; j++) {
				/* Divides out the numbers beneath the capture number,
				 * using ints to make sure the remainder is removed,
				 * then compares the result modulus 10 with 0 to pick a bucket for each.
				 */
				if ((buckets[0][j] / ((int) Math.pow(10, i))) % 10 == 0) {
					buckets[0][bucket0Size] = buckets[0][j];
					bucket0Size++;
				} else {
					buckets[1][bucket1Size] = buckets[0][j];
					bucket1Size++;
				}
			}

			for (int j = 0; j < bucket1Size; j++) {
				buckets[0][bucket0Size] = buckets[1][j];
				bucket0Size ++;
			}
			for (int each :
					buckets[0]) {
			}
		}

		for (int i = 0; i < a.length; i++) { //change array to decimal again
			a[i] = baseChange(buckets[0][i], 10, 2);
		}

		return a;
	}

	/** Takes in a decimal based number and converts it to another chosen base.
	 *
	 * @param num The decimal based number to be converted.
	 * @param base The numeric base to convert to.
	 * @return The converted number in the new base's form.
	 */
	private static int decimalToBase(int num, int base) {
		int firstNum = num % base;
		if (num / base > 0)
			return firstNum + (10 * decimalToBase(num / base, base));
		else
			return firstNum;
	}

	/** Takes in a number of any base and converts it to another chosen base.
	 *
	 * @param num The number to be converted.
	 * @param newBase The numeric base to convert to.
	 * @param oldBase The numeric base it is in right now.
	 * @return The converted number in the new base's form.
	 */
	private static int baseChange(int num, int newBase, int oldBase) {
		int firstNum = num % newBase;
		if (num / newBase > 0)
			return firstNum + (oldBase * baseChange(num / newBase, newBase, oldBase));
		else
			return firstNum;
	}
}
