package Week_13_November_12.Program_15_Sorting;

import Week_6_September_24.Programming_Contest_1.Array;

import java.util.Arrays;
import java.util.Random;

/** Program 15 Sorting p480 #10
 *
 * This sort sorts with two buckets by radix sort.
 *
 * @author Matthew Fallon
 * @since November 5, 2018
 *
 */
public class BinaryBasedSortingProgram {

    /** Main runner class. Sends a random length array of random integers
     *  at the binary search algorithm, then compares it with the array
     *  sorted by java.util.arrays sort method and prints sorted to verify.
     *
     * @param args unused.
     */

    public static void main(String[] args) {


        final int RAND = (int) (Math.random() * 1000);
        int[] a = new int[RAND];

        for (int i = 0; i < RAND; i++) {

            a[i] = (new Random().nextInt());
        }


        int[] b = Arrays.copyOf(a, a.length);
        Arrays.sort(b);



        System.out.println("Array before sort:");
        System.out.println(Arrays.toString(a));
        System.out.println("Array after sort:");
        System.out.println(Arrays.toString(binaryRadixSort(a)));

        if (Arrays.equals(b, binaryRadixSort(a))) {
            System.out.println("sorted");
        }

    }


    /**
     * Takes in an integer array, converts it to binary form and performs
     * a radix sort on the numbers before converting them back to decimal form.
     * <p>
     * O(n) complexity.
     *
     * @param a The array to be sorted.
     * @return The sorted array.
     */
    private static int[] binaryRadixSort(int[] a) {
        int[][] buckets = new int[2][a.length];
        buckets[0] = Arrays.copyOf(a, a.length);


      /*  for (int i = 0; i < a.length; i++) { //change array to binary
            buckets[0][i] = decimalToBase(a[i], 2);
        }
*/

        for (int i = 0; i < (4 * 8); i++) { //runs for each bit in an integer.
            int bucket0Size = 0;
            int bucket1Size = 0;
            for (int j = 0; j < buckets[0].length; j++) {
                /* Divides out the numbers beneath the capture number,
                 * using ints to make sure the remainder is removed,
                 * then compares the result modulus 2 with 0 to pick a bucket for each.
                 */
                if ((buckets[0][j] / ((int) Math.pow(2, i))) % 2 == 0) {
                    buckets[0][bucket0Size] = buckets[0][j];
                    bucket0Size++;
                } else {
                    buckets[1][bucket1Size] = buckets[0][j];
                    bucket1Size++;
                }
            }

            for (int j = 0; j < bucket1Size; j++) {
                buckets[0][bucket0Size] = buckets[1][j];
                bucket0Size++;
            }

        }

        int bucket0Size = 0;
        int bucket1Size = 0;
        for (int j = 0; j < buckets[0].length; j++) {
            /* Divides out the numbers beneath the capture number,
             * using ints to make sure the remainder is removed,
             * then compares the result modulus 10 with 0 to pick a bucket for each.
             */
            if (buckets[0][j] < 0) {
                buckets[0][bucket0Size] = buckets[0][j];
                bucket0Size++;
            } else {
                buckets[1][bucket1Size] = buckets[0][j];
                bucket1Size++;
            }
        }

        for (int j = 0; j < bucket0Size / 2; j++) {
            int temp = buckets[0][j];
            buckets[0][j] = buckets[0][bucket0Size - 1 - j];
            buckets[0][bucket0Size - 1 - j] = temp;
        }

        for (int j = 0; j < bucket1Size; j++) {
            buckets[0][bucket0Size] = buckets[1][j];
            bucket0Size++;
        }

      /*  for (int i = 0; i < a.length; i++) { //change array to decimal again
            a[i] = baseChange(buckets[0][i], 10, 2);
        }

*/
        a = buckets[0];

        return a;
    }
}

