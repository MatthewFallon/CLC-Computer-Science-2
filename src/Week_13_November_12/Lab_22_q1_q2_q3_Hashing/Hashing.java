package Week_13_November_12.Lab_22_q1_q2_q3_Hashing;

/**
 * @author Matthew Fallon
 * @since 11/12/2018
 */
public class Hashing {

    public static void main(String[] args) {


        String java = "Java";

        int javaHash = java.hashCode();

        final int g  = 31;

        int hornerHash = 0;
        int n = java.length();
        for (int i = 0; i < n; i++) {
            hornerHash = g * hornerHash + java.charAt(i);
        }


        assert javaHash == hornerHash;

        System.out.println("Javas hash function = " + javaHash);
        System.out.println("Horner's Hash = " + hornerHash);

        int hashIndex = javaHash % 101;

        //System.out.println(hashIndex);

        String collision = Character.toString((char) 120);
        int collisionIndex = collision.hashCode() % 101;

        assert hashIndex == collisionIndex;

        System.out.println("Java hash = " + hashIndex);
        System.out.println("Index of collision variable = " + collisionIndex);
    }
}
