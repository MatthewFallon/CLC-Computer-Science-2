package Week_11_October_29.Programming_Competition_2;

import java.util.PriorityQueue;

/** Programming Contest Question 3 Priority Queue
 * 
 * @author Matthew Fallon
 * @since 10-29-18  
 *
 */

public class Question_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PriorityQueue<String> list = new PriorityQueue<>();
		
		list.add( "Jane" );
		list.add( "Jess" );
		list.add( "Jill" );
		list.add( list.remove() );
		list.add( "Jim" );

		String name = list.remove();
		list.add( list.peek() );
		
		System.out.println(list);
	}

}
