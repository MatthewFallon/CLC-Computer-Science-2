package Week_11_October_29.Programming_Competition_2;

import java.util.ArrayDeque;

/** Programming Contest Question 2 Queue
 * 
 * @author Matthew Fallon
 * @since 10-29-18  
 *
 */

public class Question_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Question 2. Queue
		ArrayDeque<String> list = new ArrayDeque<>();
		
		list.push( "Jane" );
		list.push( "Jess" );
		list.push( "Jill" );
		list.push( list.pop() );
		list.push( "Jim" );

		String name = list.pop();
		list.push( list.peek() );
		
		System.out.println(list);
	}

}
