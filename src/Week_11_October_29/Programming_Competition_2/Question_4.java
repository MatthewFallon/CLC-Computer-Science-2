package Week_11_October_29.Programming_Competition_2;

import java.util.ArrayList;

/** Programming Contest Question 4 List
 * 
 * @author Matthew Fallon
 * @since 10-29-18  
 *
 */

public class Question_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<String> list = new ArrayList<>();
		
		list.add( "Jane" );
		list.add( "Jess" );
		list.add( "Jill" );
		list.add( list.remove(list.size() - 1) );
		list.add( "Jim" );

		String name = list.remove(list.size() - 1);
		list.add( list.get(list.size() - 1) );
	
		System.out.println(list);
	}

}
