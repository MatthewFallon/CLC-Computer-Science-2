package Week_11_October_29.Programming_Competition_2;


/** Programming Contest Question 5 Recursive method.
 * 
 * @author Matthew Fallon
 * @since 10-29-18  
 *
 */

public class Question_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num = recurse(5);
		System.out.println(num);
	}
	
	private static int recurse(int i) {
		
		/* t(1) = 2

		t(n) = 1 + t(n + 1) for n > 1
		*/
		if (i == 1) {
			return 2;
		}
		else {
			return 1 + recurse(i - 1);
		}
		
	}

}
