package Week_11_October_29.Exam_2;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

/** Problem a) from exam 2
 * @author Matthew Fallon
 * @since 10/31/2018
 */
public class Problem_a {

    public static void main(String[] args) {

        Stack<Integer> s = new Stack<>();//initialization

        s.push(3);//part i
        s.push(33);
        s.push(333);
        s.push(3333);
        s.push(33333);


        System.out.println("Using an array.");
        System.out.println(s);

        int [] arr = new int[s.size()];

        int i = 0;
        while (! s.empty()) {
            arr[i] = s.pop();
            i++;
        }

        i = 0;
        while (i < arr.length) {
            s.push(arr[i]);
            i++;
        }

        System.out.println(s);//end of part i




        System.out.println();//spacing
        System.out.println("Using two extra stacks.");//part ii

        s.clear(); // clear to restart list
        s.push(3);
        s.push(33);
        s.push(333);
        s.push(3333);
        s.push(33333);

        System.out.println(s);


        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();


        while (! s.empty()) {
            s1.push(s.pop());
        }

        while (! s1.empty()) {
            s2.push(s1.pop());
        }

        while (! s2.empty()) {
            s.push(s2.pop());
        }

        System.out.println(s);//end of part ii

         System.out.println();//spacing
        System.out.println("Using a queue.");//part iii

        s.clear(); // clear to restart list
        s.push(3);
        s.push(33);
        s.push(333);
        s.push(3333);
        s.push(33333);

        System.out.println(s);


        Queue<Integer> q1 = new ArrayDeque<>();

        while (! s.empty()) {
            q1.add(s.pop());
        }

        while (! q1.isEmpty()) {
            s.push(q1.remove());
        }

        System.out.println(s);//end of part iii








    }
}
