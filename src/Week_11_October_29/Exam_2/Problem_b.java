package Week_11_October_29.Exam_2;

/** Problem a from exam 2
 * @author Matthew Fallon
 * @since 10/31/2018
 */
public class Problem_b {

    public static void main(String[] args) {

        System.out.println("s(" + 5 + "): " + s(5));
        System.out.println("s(" + 10 + "): " + s(10));


       /* for (int i = 0; i <= 10; i++) {
            System.out.println("s(" + i + "): " + s(i));
        }*/

    }

    public static double s(double n) {
        if(n == 0) {
            return 1;
        }
        else {
            double a = s(n-1);
            return a + (1 / a);
        }
    }
}
