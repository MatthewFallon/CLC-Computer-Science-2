package Week_12_November_5.Lab_19_Search_Q1;

/** Lab practicing search algorithm.
 * 
 * @author Matthew Fallon
 * @since November 5, 2018
 * @author Frank M. Carrano, Timothy M. Henry
 * @version 5.0
 *
 */

public class SearchQ1 {

	public static void main(String[] args) {
		Integer[] a0 = {9, 5, 8, 4, 7};
		System.out.println(inArray(a0, 8));
		System.out.println(inArray(a0, 6));
		
		Integer[] a1 = {1, 1, 3, 3, 5, 5, 7, 7, 9, 9};
		System.out.println(inArray(a1, 7));
		System.out.println(inArray(a1, 8));

	}
	
	public static <T> Integer inArray(T[] anArray, T anEntry)
	{
	   boolean found = false;
	   int index = 0;
	   int result = -1;
	   while (!found && (index < anArray.length))
	   {
	      if (anEntry.equals(anArray[index])) {
	         found = true;
	         result = index;
	      }
	      index++;
	   } // end while

	   return result;
	} // end inArray

}
