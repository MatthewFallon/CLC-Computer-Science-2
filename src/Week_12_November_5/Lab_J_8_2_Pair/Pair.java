package Week_12_November_5.Lab_J_8_2_Pair;

/** Pair Lab project
 *
 * @author Matthew Fallon
 * @since November 7, 2018
 * @param <S> First object in pair
 * @param <T> Second generic object in pair.
 */

public class Pair<S, T> {
	
	private S first;
	private T second;
	
	public Pair(S firstItem, T secondItem ){
		first = firstItem;
		second = secondItem;
	}

	public String toString(){
		return "(" + first + ", " + second + ")";
	}

	public static void main(String[] args) {
		Name reed = new Name("Joe", "Java");
		String phone = "(402) 555-1234";
		Pair<Name, String> p0 = new Pair<>(reed, phone);

		System.out.println(p0);
	}
}

