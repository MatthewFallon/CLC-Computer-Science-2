package Week_12_November_5.Lab_19_13_Binary_Search;

import java.util.Arrays;

/** Lab to demonstrate binary search.
 * 
 * @author Matthew Fallon
 * @since November 5, 2018
 *
 */

public class BinarySearchLab {

	public static void main(String[] args) {
		Integer[] a = {2,4,5,7,8,10,12,15,18,21,24,26};
		System.out.println(inArray(a, 7));
		System.out.println(inArray(a, 15));
		System.out.println(inArray(a, 8));
		
		System.out.println(Arrays.binarySearch(a, 0, a.length, 7));

	}

	private static <T extends Comparable<? super T>>
	boolean binarySearch(T[] anArray, int first, int last, T desiredItem)
	{
		boolean found;
		int mid = first + (last - first) / 2;

		if (first > last)
			found = false;
		else if (desiredItem.equals(anArray[mid]))
			found = true;
		else if (desiredItem.compareTo(anArray[mid]) < 0)
			found = binarySearch(anArray, first, mid - 1, desiredItem);
		else
			found = binarySearch(anArray, mid + 1, last, desiredItem);

		return found;
	} // end binarySearch

	public static <T extends Comparable<? super T>> boolean inArray(T[] anArray, T anEntry)
	{
		return binarySearch(anArray, 0, anArray.length - 1, anEntry);
	} // end inArray

}
