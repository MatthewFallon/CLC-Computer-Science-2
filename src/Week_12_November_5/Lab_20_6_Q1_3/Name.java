package Week_12_November_5.Lab_20_6_Q1_3;

/**
   A class that represents a person's name.
   The class in Listing B-1 in Segment B.16 of Appendix B,
   but modified to use and interface. NOTE the change to the
   parameter in the method giveLastNameTo.
   @author Frank M. Carrano
   @author Timothy M. Henry
   @version 5.0
*/
public class Name implements NameInterface
{
	private String first; // First name
	private String last;  // Last name

	public Name()
	{
	} // end default constructor

	public Name(String firstName, String lastName)
	{
		first = firstName;
		last = lastName;
	} // end constructor

	public void setName(String firstName, String lastName)
	{
		setFirst(firstName);
		setLast(lastName);
	} // end setName

	public String getName()
	{
		return toString();
	} // end getName

	public void setFirst(String firstName)
	{
		first = firstName; 
	} // end setFirst

	public String getFirst()
	{
		return first;
	} // end getFirst

	public void setLast(String lastName)
	{
		last = lastName;
	} // end setLast

	public String getLast()
	{
		return last;
	} // end getLast

   public void giveLastNameTo(NameInterface aName)
	{
		aName.setLast(last);
	} // end giveLastNameTo

	public String toString()
	{
		return first + " " + last;
	} // end toString

	@Override
	public boolean equals(Name newName) {
		if (this.getFirst().equals(newName.getFirst()) && this.getLast().equals(newName.getLast())){
			return true;
		} else {
			return false;
		}
	}
} // end Name
